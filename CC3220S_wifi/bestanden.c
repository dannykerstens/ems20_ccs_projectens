/*
 * fs.c
 *
 *  Created on: 20 jun. 2018
 *      Author: VersD
 */

#include "bestanden.h"


_u32 g_tokens[4] = {0xAB34CD78, 0, 0, 0};
_u32 g_accessMode = SL_FS_CREATE_SECURE | SL_FS_CREATE_NOSIGNATURE |
                    SL_FS_CREATE_VENDOR_TOKEN | SL_FS_CREATE_STATIC_TOKEN;


int leesWachtwoord(char * ww, bool vraagOmWachtwoord)
{
    /* Als wachtwoord nog niet is opgeslagen, alsnog doen, anders ophalen */
    int Status=-1;
    _i32 bestand;

    if((getFileInfo()==SL_ERROR_FS_FILE_NOT_EXISTS) || vraagOmWachtwoord==true)        //bestand bestaat niet, aanmaken, dit is eenmalig
    {
        UART_PRINT("\r\nGeen wachtwoord gevonden.\r\n Voer jouw wifi wachtwoord in:\r\n");
        char c,i=0;
        while((c=getch())!='\n' && c!=0x0d) //zolang geen enter binnenkomt
        {
            ww[i++]=c;
        }
        ww[i]='\0';
        Status = schrijfWachtwoord(ww);
        UART_PRINT("Wachtwoord ingevoerd.\r\n");

    }else{   //bestand bestaat wel, uitlezen.
        bestand=sl_FsOpen(BESTANDSNAAM,SL_FS_READ, &g_tokens[SL_FS_TOKEN_READ_ONLY]);
        if(bestand<0)
        {
            UART_PRINT("Fout bij het openen van het wachtwoordbestand: %d\r\n",bestand);
        }else{
            Status = sl_FsRead(bestand, 0, (unsigned char *)ww, 64);
            Status += sl_FsClose(bestand, 0, 0, 0);
        }
    }

    return Status;
}

int schrijfWachtwoord(char * ww)
{
    /* Als wachtwoord nog niet is opgeslagen, alsnog doen, anders ophalen */
    int Status=-1;
    _i32 bestand;

    bestand =  sl_FsOpen(BESTANDSNAAM,
                     (SL_FS_CREATE | SL_FS_CREATE_FAILSAFE | SL_FS_OVERWRITE | SL_FS_CREATE_MAX_SIZE(65) | g_accessMode),
                     &g_tokens[SL_FS_TOKEN_MASTER]);

    if(bestand<0)
    {
        UART_PRINT("Fout bij het openen van het wachtwoordbestand: %d\r\n",bestand);
    }else{

        Status = sl_FsWrite(bestand, 0, (unsigned char *)ww, strlen(ww));
        Status += sl_FsClose(bestand, 0, 0, 0);
        Status += getFileInfo(); //update tokens
    }
    return Status;
}

/* Call this function after the secure file is created */
int getFileInfo()
{
   SlFsFileInfo_t fileInfo;
   int RetVal;

   RetVal = sl_FsGetInfo(BESTANDSNAAM, g_tokens[SL_FS_TOKEN_MASTER], &fileInfo);
   if(0 == RetVal)
   {
       g_tokens[SL_FS_TOKEN_READ_ONLY] = fileInfo.Token[SL_FS_TOKEN_READ_ONLY];
       g_tokens[SL_FS_TOKEN_WRITE_ONLY] = fileInfo.Token[SL_FS_TOKEN_WRITE_ONLY];
       g_tokens[SL_FS_TOKEN_WRITE_READ] = fileInfo.Token[SL_FS_TOKEN_WRITE_READ];
   }
   return RetVal;
}
